let html = ``;

const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];


data.forEach(function(d) {
    html = html + `
    <article class="pizzaThumbnail">
        <a href="${d.image}">
            <img src="${d.image}"/>
            <section>
                <h4>${d.name}</h4>
                <ul>
                    <li>Prix petit format : ${d.price_small} €</li>
                    <li>Prix grand format : ${d.price_large} €</li>
                </ul>
            </section>
        </a>
    </article>`;
});
console.log(html)
document.querySelector('.pageContent').innerHTML = html;
